# AlarmClock

A standalone (no smartphone) alarm clock with separate alarms for each day of the week, support for custom alarm sounds from an SD card and an ambient light sensor for automatic display brightness control. It also has an interface for peripheral devices, such as a variable color temperature LED light for simulating sunrise. This project is still in development and exists so far only in CAD, apart from some material and process studies.

<img src="images/AlarmClock_CAD.png" height="300" />
<img src="images/SunriseLight_CAD.png" height="300" />

*CAD models of the alarm clock and the separate sunrise light*

The alarm clock enclosure is intended to be laser-cut plywood, while the sunrise light's outer ring will be 3D printed. The dome is a repurposed diffusor dome of the Astera AX3 spotlight (https://astera-led.com/ax3/).

![Main board schematic](images/Schematic_Main.png)

*The schematic of the main PCB*

## PCB 3D renders

<img src="KiCad/Display/render.png" height="600" />

*The display PCB with reverse-mount 7-segment LED displays and LEDs diffused by the PCB for the weekday and alarm symbols*

<img src="KiCad/SunriseLight/render.png" height="300" />

*The circuit board of the sunrise light with footprints for 5000 K, 2700 K and red LEDs*

