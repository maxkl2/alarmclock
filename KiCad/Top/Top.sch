EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 3250 2850
Wire Wire Line
	3250 2950 3250 2850
Wire Wire Line
	3850 2950 3650 2950
Wire Wire Line
	3850 2000 3850 2950
Wire Wire Line
	3250 2000 3850 2000
Wire Wire Line
	3250 2850 3250 2000
Wire Wire Line
	1300 2350 3450 2350
Wire Wire Line
	1300 2650 3450 2650
$Comp
L CustomSymbols:CapTouch_Pad P?
U 1 1 6021C0B2
P 3600 2650
AR Path="/5FEBF294/6021C0B2" Ref="P?"  Part="1" 
AR Path="/6021C0B2" Ref="P4"  Part="1" 
F 0 "P4" H 3450 2600 50  0000 L CNN
F 1 "CapTouch_Pad" H 3700 2650 50  0000 L CNN
F 2 "CustomFootprints:CapTouch_Pad_12mm_Hole5mm" H 3600 2650 50  0001 C CNN
F 3 "" H 3600 2650 50  0001 C CNN
	1    3600 2650
	1    0    0    -1  
$EndComp
$Comp
L CustomSymbols:CapTouch_Pad P?
U 1 1 6021C0AC
P 3500 2550
AR Path="/5FEBF294/6021C0AC" Ref="P?"  Part="1" 
AR Path="/6021C0AC" Ref="P2"  Part="1" 
F 0 "P2" H 3350 2600 50  0000 L CNN
F 1 "CapTouch_Pad" H 3600 2550 50  0000 L CNN
F 2 "CustomFootprints:CapTouch_Pad_12mm_Hole5mm" H 3500 2550 50  0001 C CNN
F 3 "" H 3500 2550 50  0001 C CNN
	1    3500 2550
	1    0    0    -1  
$EndComp
$Comp
L CustomSymbols:CapTouch_Pad P?
U 1 1 6021C0A6
P 3600 2350
AR Path="/5FEBF294/6021C0A6" Ref="P?"  Part="1" 
AR Path="/6021C0A6" Ref="P3"  Part="1" 
F 0 "P3" H 3450 2300 50  0000 L CNN
F 1 "CapTouch_Pad" H 3700 2350 50  0000 L CNN
F 2 "CustomFootprints:CapTouch_Pad_12mm_Hole5mm" H 3600 2350 50  0001 C CNN
F 3 "" H 3600 2350 50  0001 C CNN
	1    3600 2350
	1    0    0    -1  
$EndComp
$Comp
L CustomSymbols:CapTouch_Pad P?
U 1 1 6021C0A0
P 3500 2250
AR Path="/5FEBF294/6021C0A0" Ref="P?"  Part="1" 
AR Path="/6021C0A0" Ref="P1"  Part="1" 
F 0 "P1" H 3350 2300 50  0000 L CNN
F 1 "CapTouch_Pad" H 3600 2250 50  0000 L CNN
F 2 "CustomFootprints:CapTouch_Pad_12mm_Hole5mm" H 3500 2250 50  0001 C CNN
F 3 "" H 3500 2250 50  0001 C CNN
	1    3500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2850 3250 2850
Text Notes 1550 1900 0    50   ~ 0
Max current: 40mA
Text Notes 5150 4250 0    50   ~ 0
Bypass caps to not disturb cap touch (see ST appnote AN4312)
Text Notes 5150 4050 0    50   ~ 0
See front PCB for LED current details
Text Notes 4900 3650 0    50   ~ 0
Red reverse mount LEDs, diffused by PCB
Text Notes 3200 1850 0    50   ~ 0
Actively driven shield electrode
Connection ~ 3150 2950
Connection ~ 3150 2150
Wire Wire Line
	3150 3050 3150 2950
Wire Wire Line
	3950 3050 3150 3050
Wire Wire Line
	3950 1900 3950 3050
Wire Wire Line
	3150 1900 3950 1900
Wire Wire Line
	3150 2150 3150 1900
$Comp
L power:GND #PWR?
U 1 1 6021C08C
P 2450 4050
AR Path="/5FEBF294/6021C08C" Ref="#PWR?"  Part="1" 
AR Path="/6021C08C" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 2450 3800 50  0001 C CNN
F 1 "GND" H 2455 3877 50  0000 C CNN
F 2 "" H 2450 4050 50  0001 C CNN
F 3 "" H 2450 4050 50  0001 C CNN
	1    2450 4050
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 6021C086
P 2450 3750
AR Path="/5FEBF294/6021C086" Ref="#PWR?"  Part="1" 
AR Path="/6021C086" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2450 3600 50  0001 C CNN
F 1 "+3V3" H 2465 3923 50  0000 C CNN
F 2 "" H 2450 3750 50  0001 C CNN
F 3 "" H 2450 3750 50  0001 C CNN
	1    2450 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6021C080
P 2450 3900
AR Path="/5FEBF294/6021C080" Ref="C?"  Part="1" 
AR Path="/6021C080" Ref="C1"  Part="1" 
F 0 "C1" H 2565 3946 50  0000 L CNN
F 1 "100n" H 2565 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2488 3750 50  0001 C CNN
F 3 "~" H 2450 3900 50  0001 C CNN
	1    2450 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6021C07A
P 2900 4300
AR Path="/5FEBF294/6021C07A" Ref="#PWR?"  Part="1" 
AR Path="/6021C07A" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 2900 4050 50  0001 C CNN
F 1 "GND" H 2905 4127 50  0000 C CNN
F 2 "" H 2900 4300 50  0001 C CNN
F 3 "" H 2900 4300 50  0001 C CNN
	1    2900 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4300 4250 4200
Wire Wire Line
	2900 4300 2900 4200
$Comp
L power:GND #PWR?
U 1 1 6021C072
P 4250 4300
AR Path="/5FEBF294/6021C072" Ref="#PWR?"  Part="1" 
AR Path="/6021C072" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 4250 4050 50  0001 C CNN
F 1 "GND" H 4255 4127 50  0000 C CNN
F 2 "" H 4250 4300 50  0001 C CNN
F 3 "" H 4250 4300 50  0001 C CNN
	1    4250 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4200 4800 4300
Wire Wire Line
	3450 4200 3450 4300
$Comp
L power:GND #PWR?
U 1 1 6021C06A
P 3450 4300
AR Path="/5FEBF294/6021C06A" Ref="#PWR?"  Part="1" 
AR Path="/6021C06A" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 3450 4050 50  0001 C CNN
F 1 "GND" H 3455 4127 50  0000 C CNN
F 2 "" H 3450 4300 50  0001 C CNN
F 3 "" H 3450 4300 50  0001 C CNN
	1    3450 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6021C064
P 4800 4300
AR Path="/5FEBF294/6021C064" Ref="#PWR?"  Part="1" 
AR Path="/6021C064" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 4800 4050 50  0001 C CNN
F 1 "GND" H 4805 4127 50  0000 C CNN
F 2 "" H 4800 4300 50  0001 C CNN
F 3 "" H 4800 4300 50  0001 C CNN
	1    4800 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3900 2900 3850
Wire Wire Line
	3100 3450 3650 3450
Wire Wire Line
	3450 3900 3450 3850
Wire Wire Line
	4050 3450 4600 3450
Wire Wire Line
	4250 3900 4250 3850
Wire Wire Line
	4800 3900 4800 3850
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 6021C058
P 3750 4600
AR Path="/5FEBF294/6021C058" Ref="Q?"  Part="1" 
AR Path="/6021C058" Ref="Q1"  Part="1" 
F 0 "Q1" H 3954 4646 50  0000 L CNN
F 1 "2N7002P" H 3954 4555 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3950 4700 50  0001 C CNN
F 3 "~" H 3750 4600 50  0001 C CNN
	1    3750 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6021C052
P 3150 4950
AR Path="/5FEBF294/6021C052" Ref="#PWR?"  Part="1" 
AR Path="/6021C052" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 3150 4700 50  0001 C CNN
F 1 "GND" H 3155 4777 50  0000 C CNN
F 2 "" H 3150 4950 50  0001 C CNN
F 3 "" H 3150 4950 50  0001 C CNN
	1    3150 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4600 2150 4600
Connection ~ 3150 4600
Wire Wire Line
	3150 4650 3150 4600
Wire Wire Line
	3250 4600 3150 4600
$Comp
L Device:R R?
U 1 1 6021C048
P 3150 4800
AR Path="/5FEBF294/6021C048" Ref="R?"  Part="1" 
AR Path="/6021C048" Ref="R2"  Part="1" 
F 0 "R2" H 3220 4846 50  0000 L CNN
F 1 "100k" H 3220 4755 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3080 4800 50  0001 C CNN
F 3 "~" H 3150 4800 50  0001 C CNN
	1    3150 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6021C042
P 3400 4600
AR Path="/5FEBF294/6021C042" Ref="R?"  Part="1" 
AR Path="/6021C042" Ref="R3"  Part="1" 
F 0 "R3" V 3500 4700 50  0000 C CNN
F 1 "10" V 3400 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3330 4600 50  0001 C CNN
F 3 "~" H 3400 4600 50  0001 C CNN
	1    3400 4600
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 6021C03C
P 3850 3350
AR Path="/5FEBF294/6021C03C" Ref="#PWR?"  Part="1" 
AR Path="/6021C03C" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 3850 3200 50  0001 C CNN
F 1 "+3V3" H 3865 3523 50  0000 C CNN
F 2 "" H 3850 3350 50  0001 C CNN
F 3 "" H 3850 3350 50  0001 C CNN
	1    3850 3350
	1    0    0    -1  
$EndComp
Connection ~ 4600 3850
Wire Wire Line
	4600 3850 4600 3800
Wire Wire Line
	4600 3850 4600 3900
Wire Wire Line
	4800 3850 4600 3850
Wire Wire Line
	4050 3850 4050 3900
Connection ~ 4050 3850
Wire Wire Line
	4250 3850 4050 3850
Wire Wire Line
	4050 3800 4050 3850
Connection ~ 3650 3850
Wire Wire Line
	3650 3850 3650 3900
Wire Wire Line
	3650 3850 3650 3800
Wire Wire Line
	3450 3850 3650 3850
Connection ~ 3100 3850
Wire Wire Line
	3100 3850 3100 3900
Wire Wire Line
	3100 3850 3100 3800
Wire Wire Line
	2900 3850 3100 3850
$Comp
L Device:C C?
U 1 1 6021C026
P 4800 4050
AR Path="/5FEBF294/6021C026" Ref="C?"  Part="1" 
AR Path="/6021C026" Ref="C5"  Part="1" 
F 0 "C5" H 4915 4096 50  0000 L CNN
F 1 "10n" H 4915 4005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4838 3900 50  0001 C CNN
F 3 "~" H 4800 4050 50  0001 C CNN
	1    4800 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6021C020
P 4250 4050
AR Path="/5FEBF294/6021C020" Ref="C?"  Part="1" 
AR Path="/6021C020" Ref="C4"  Part="1" 
F 0 "C4" H 4365 4096 50  0000 L CNN
F 1 "10n" H 4365 4005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4288 3900 50  0001 C CNN
F 3 "~" H 4250 4050 50  0001 C CNN
	1    4250 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6021C01A
P 3450 4050
AR Path="/5FEBF294/6021C01A" Ref="C?"  Part="1" 
AR Path="/6021C01A" Ref="C3"  Part="1" 
F 0 "C3" H 3565 4096 50  0000 L CNN
F 1 "10n" H 3565 4005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3488 3900 50  0001 C CNN
F 3 "~" H 3450 4050 50  0001 C CNN
	1    3450 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6021C014
P 2900 4050
AR Path="/5FEBF294/6021C014" Ref="C?"  Part="1" 
AR Path="/6021C014" Ref="C2"  Part="1" 
F 0 "C2" H 2785 4096 50  0000 R CNN
F 1 "10n" H 2785 4005 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2938 3900 50  0001 C CNN
F 3 "~" H 2900 4050 50  0001 C CNN
	1    2900 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2050 2150 2050
Wire Wire Line
	2150 4600 2150 2050
$Comp
L power:GND #PWR?
U 1 1 6021C00C
P 3850 4800
AR Path="/5FEBF294/6021C00C" Ref="#PWR?"  Part="1" 
AR Path="/6021C00C" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 3850 4550 50  0001 C CNN
F 1 "GND" H 3855 4627 50  0000 C CNN
F 2 "" H 3850 4800 50  0001 C CNN
F 3 "" H 3850 4800 50  0001 C CNN
	1    3850 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3450 4050 3450
Connection ~ 3850 3450
Wire Wire Line
	3850 3450 3850 3350
Wire Wire Line
	3650 3450 3850 3450
Connection ~ 3650 3450
Wire Wire Line
	3650 3450 3650 3500
Connection ~ 4050 3450
Wire Wire Line
	4050 3450 4050 3500
Wire Wire Line
	4600 3450 4600 3500
Wire Wire Line
	3100 3500 3100 3450
Wire Wire Line
	3850 4250 4050 4250
Connection ~ 3850 4250
Wire Wire Line
	3850 4250 3850 4400
Wire Wire Line
	4050 4250 4600 4250
Connection ~ 4050 4250
Wire Wire Line
	4050 4200 4050 4250
Wire Wire Line
	3650 4250 3850 4250
Connection ~ 3650 4250
Wire Wire Line
	3650 4200 3650 4250
Wire Wire Line
	4600 4250 4600 4200
Wire Wire Line
	3100 4250 3650 4250
Wire Wire Line
	3100 4200 3100 4250
$Comp
L Device:R R?
U 1 1 6021BFF0
P 4600 4050
AR Path="/5FEBF294/6021BFF0" Ref="R?"  Part="1" 
AR Path="/6021BFF0" Ref="R6"  Part="1" 
F 0 "R6" H 4670 4096 50  0000 L CNN
F 1 "140" H 4670 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4530 4050 50  0001 C CNN
F 3 "~" H 4600 4050 50  0001 C CNN
	1    4600 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6021BFEA
P 4050 4050
AR Path="/5FEBF294/6021BFEA" Ref="R?"  Part="1" 
AR Path="/6021BFEA" Ref="R5"  Part="1" 
F 0 "R5" H 4120 4096 50  0000 L CNN
F 1 "140" H 4120 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3980 4050 50  0001 C CNN
F 3 "~" H 4050 4050 50  0001 C CNN
	1    4050 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6021BFE4
P 3650 4050
AR Path="/5FEBF294/6021BFE4" Ref="R?"  Part="1" 
AR Path="/6021BFE4" Ref="R4"  Part="1" 
F 0 "R4" H 3720 4096 50  0000 L CNN
F 1 "140" H 3720 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3580 4050 50  0001 C CNN
F 3 "~" H 3650 4050 50  0001 C CNN
	1    3650 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6021BFDE
P 3100 4050
AR Path="/5FEBF294/6021BFDE" Ref="R?"  Part="1" 
AR Path="/6021BFDE" Ref="R1"  Part="1" 
F 0 "R1" H 3170 4096 50  0000 L CNN
F 1 "140" H 3170 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 4050 50  0001 C CNN
F 3 "~" H 3100 4050 50  0001 C CNN
	1    3100 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2250 3350 2250
Wire Wire Line
	1300 2550 3350 2550
Wire Wire Line
	3150 2450 3150 2750
Connection ~ 3150 2450
Wire Wire Line
	1300 2450 3150 2450
Wire Wire Line
	3150 2750 3150 2950
Connection ~ 3150 2750
Wire Wire Line
	1300 2750 3150 2750
Wire Wire Line
	3150 2950 1300 2950
Wire Wire Line
	3150 2150 3150 2450
Wire Wire Line
	1300 2150 3150 2150
Text Notes 1300 2950 0    50   ~ 0
SHIELD
Text Notes 1300 2750 0    50   ~ 0
SHIELD
Text Notes 1300 2450 0    50   ~ 0
SHIELD
Wire Wire Line
	1350 3050 1350 3100
Wire Wire Line
	1300 3050 1350 3050
Wire Wire Line
	1350 1950 1350 1900
Wire Wire Line
	1300 1950 1350 1950
$Comp
L power:+3V3 #PWR?
U 1 1 6021BFC0
P 1350 1900
AR Path="/5FEBF294/6021BFC0" Ref="#PWR?"  Part="1" 
AR Path="/6021BFC0" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 1350 1750 50  0001 C CNN
F 1 "+3V3" H 1365 2073 50  0000 C CNN
F 2 "" H 1350 1900 50  0001 C CNN
F 3 "" H 1350 1900 50  0001 C CNN
	1    1350 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6021BFBA
P 1350 3100
AR Path="/5FEBF294/6021BFBA" Ref="#PWR?"  Part="1" 
AR Path="/6021BFBA" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 1350 2850 50  0001 C CNN
F 1 "GND" H 1355 2927 50  0000 C CNN
F 2 "" H 1350 3100 50  0001 C CNN
F 3 "" H 1350 3100 50  0001 C CNN
	1    1350 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x12 J?
U 1 1 6021BFB4
P 1100 2450
AR Path="/5FEBF294/6021BFB4" Ref="J?"  Part="1" 
AR Path="/6021BFB4" Ref="J1"  Part="1" 
F 0 "J1" H 1179 2442 50  0000 L CNN
F 1 "Conn_01x12" H 1179 2351 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical_SMD" H 1100 2450 50  0001 C CNN
F 3 "~" H 1100 2450 50  0001 C CNN
	1    1100 2450
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 6021BFAE
P 4600 3650
AR Path="/5FEBF294/6021BFAE" Ref="D?"  Part="1" 
AR Path="/6021BFAE" Ref="D4"  Part="1" 
F 0 "D4" V 4639 3532 50  0000 R CNN
F 1 "LED" V 4548 3532 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount_NoHole" H 4600 3650 50  0001 C CNN
F 3 "~" H 4600 3650 50  0001 C CNN
	1    4600 3650
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 6021BFA8
P 4050 3650
AR Path="/5FEBF294/6021BFA8" Ref="D?"  Part="1" 
AR Path="/6021BFA8" Ref="D3"  Part="1" 
F 0 "D3" V 4089 3532 50  0000 R CNN
F 1 "LED" V 3998 3532 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount_NoHole" H 4050 3650 50  0001 C CNN
F 3 "~" H 4050 3650 50  0001 C CNN
	1    4050 3650
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 6021BFA2
P 3650 3650
AR Path="/5FEBF294/6021BFA2" Ref="D?"  Part="1" 
AR Path="/6021BFA2" Ref="D2"  Part="1" 
F 0 "D2" V 3689 3532 50  0000 R CNN
F 1 "LED" V 3598 3532 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount_NoHole" H 3650 3650 50  0001 C CNN
F 3 "~" H 3650 3650 50  0001 C CNN
	1    3650 3650
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 6021BF9C
P 3100 3650
AR Path="/5FEBF294/6021BF9C" Ref="D?"  Part="1" 
AR Path="/6021BF9C" Ref="D1"  Part="1" 
F 0 "D1" V 3139 3532 50  0000 R CNN
F 1 "LED" V 3048 3532 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount_NoHole" H 3100 3650 50  0001 C CNN
F 3 "~" H 3100 3650 50  0001 C CNN
	1    3100 3650
	0    -1   -1   0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60249272
P 1800 3600
F 0 "#FLG0101" H 1800 3675 50  0001 C CNN
F 1 "PWR_FLAG" H 1800 3773 50  0000 C CNN
F 2 "" H 1800 3600 50  0001 C CNN
F 3 "~" H 1800 3600 50  0001 C CNN
	1    1800 3600
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 60249717
P 1400 3600
F 0 "#FLG0102" H 1400 3675 50  0001 C CNN
F 1 "PWR_FLAG" H 1400 3773 50  0000 C CNN
F 2 "" H 1400 3600 50  0001 C CNN
F 3 "~" H 1400 3600 50  0001 C CNN
	1    1400 3600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60249AEC
P 1800 3600
AR Path="/5FEBF294/60249AEC" Ref="#PWR?"  Part="1" 
AR Path="/60249AEC" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 1800 3350 50  0001 C CNN
F 1 "GND" H 1805 3427 50  0000 C CNN
F 2 "" H 1800 3600 50  0001 C CNN
F 3 "" H 1800 3600 50  0001 C CNN
	1    1800 3600
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60249DC7
P 1400 3600
AR Path="/5FEBF294/60249DC7" Ref="#PWR?"  Part="1" 
AR Path="/60249DC7" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 1400 3450 50  0001 C CNN
F 1 "+3V3" H 1415 3773 50  0000 C CNN
F 2 "" H 1400 3600 50  0001 C CNN
F 3 "" H 1400 3600 50  0001 C CNN
	1    1400 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT1
U 1 1 6056849A
P 3550 2950
F 0 "NT1" H 3550 3131 50  0000 C CNN
F 1 "Net-Tie_2" H 3550 3040 50  0000 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad2.0mm" H 3550 2950 50  0001 C CNN
F 3 "~" H 3550 2950 50  0001 C CNN
	1    3550 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2950 3250 2950
Text Label 1300 2050 0    50   ~ 0
LED_CTRL
Text Label 1300 2150 0    50   ~ 0
SHIELD
Text Label 1300 2250 0    50   ~ 0
BTN_1
Text Label 1300 2350 0    50   ~ 0
BTN_3
Text Label 1300 2550 0    50   ~ 0
BTN_2
Text Label 1300 2650 0    50   ~ 0
BTN_4
Text Label 1300 2850 0    50   ~ 0
PROX
$EndSCHEMATC
