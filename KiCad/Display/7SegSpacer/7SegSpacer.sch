EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2600 2750 2400 2750
Wire Wire Line
	2600 2650 2300 2650
Wire Wire Line
	2600 2550 2200 2550
Wire Wire Line
	2600 2450 2100 2450
Wire Wire Line
	2600 2350 2000 2350
Wire Wire Line
	2600 2250 1900 2250
Wire Wire Line
	2600 2150 1800 2150
Wire Wire Line
	4000 2750 3900 2750
Wire Wire Line
	4000 2650 3800 2650
Wire Wire Line
	4000 2550 3700 2550
Wire Wire Line
	4000 2450 3600 2450
Wire Wire Line
	4000 2350 3500 2350
Wire Wire Line
	4000 2250 3400 2250
Wire Wire Line
	4000 2150 3300 2150
Wire Wire Line
	5400 2750 5300 2750
Wire Wire Line
	5400 2650 5200 2650
Wire Wire Line
	5400 2550 5100 2550
Wire Wire Line
	5400 2450 5000 2450
Wire Wire Line
	5400 2350 4900 2350
Wire Wire Line
	5400 2250 4800 2250
Wire Wire Line
	5400 2150 4700 2150
Wire Wire Line
	6800 2750 6700 2750
Wire Wire Line
	6800 2650 6600 2650
Wire Wire Line
	6800 2550 6500 2550
Wire Wire Line
	6800 2450 6400 2450
Wire Wire Line
	6800 2350 6300 2350
Wire Wire Line
	6800 2250 6200 2250
Wire Wire Line
	6800 2150 6100 2150
Wire Wire Line
	1800 3050 3300 3050
Wire Wire Line
	1800 2150 1800 3050
Wire Wire Line
	3300 2150 3300 3050
Wire Wire Line
	1900 2250 1900 3150
Wire Wire Line
	3400 2250 3400 3150
Wire Wire Line
	2000 2350 2000 3250
Wire Wire Line
	3500 2350 3500 3250
Wire Wire Line
	2100 3350 3600 3350
Wire Wire Line
	2100 2450 2100 3350
Wire Wire Line
	3600 2450 3600 3350
Wire Wire Line
	2200 3450 3700 3450
Wire Wire Line
	2200 2550 2200 3450
Wire Wire Line
	3700 2550 3700 3450
Wire Wire Line
	2300 3550 3800 3550
Wire Wire Line
	2300 2650 2300 3550
Wire Wire Line
	3800 2650 3800 3550
Wire Wire Line
	2400 3650 3900 3650
Wire Wire Line
	2400 2750 2400 3650
Wire Wire Line
	3900 2750 3900 3650
Wire Wire Line
	2500 3750 4000 3750
Wire Wire Line
	2500 2850 2500 3750
Wire Wire Line
	4000 2850 4000 3750
Wire Wire Line
	3300 3050 4700 3050
Wire Wire Line
	6100 2150 6100 3050
Connection ~ 3300 3050
Wire Wire Line
	3400 3150 4800 3150
Wire Wire Line
	6200 2250 6200 3150
Connection ~ 3400 3150
Wire Wire Line
	3500 3250 4900 3250
Wire Wire Line
	6300 2350 6300 3250
Connection ~ 3500 3250
Wire Wire Line
	6400 2450 6400 3350
Connection ~ 3600 3350
Wire Wire Line
	3700 3450 5100 3450
Wire Wire Line
	6500 2550 6500 3450
Connection ~ 3700 3450
Wire Wire Line
	3800 3550 5200 3550
Wire Wire Line
	6600 2650 6600 3550
Connection ~ 3800 3550
Wire Wire Line
	3900 3650 5300 3650
Wire Wire Line
	6700 2750 6700 3650
Connection ~ 3900 3650
Wire Wire Line
	4000 3750 5400 3750
Wire Wire Line
	6800 2850 6800 3750
Connection ~ 4000 3750
Wire Wire Line
	4700 2150 4700 3050
Connection ~ 4700 3050
Wire Wire Line
	4700 3050 6100 3050
Wire Wire Line
	4800 2250 4800 3150
Connection ~ 4800 3150
Wire Wire Line
	4800 3150 6200 3150
Wire Wire Line
	4900 2350 4900 3250
Connection ~ 4900 3250
Wire Wire Line
	4900 3250 6300 3250
Wire Wire Line
	5000 2450 5000 3350
Connection ~ 5000 3350
Wire Wire Line
	5000 3350 6400 3350
Wire Wire Line
	5100 2550 5100 3450
Connection ~ 5100 3450
Wire Wire Line
	5100 3450 6500 3450
Wire Wire Line
	5200 2650 5200 3550
Connection ~ 5200 3550
Wire Wire Line
	5300 2750 5300 3650
Connection ~ 5300 3650
Wire Wire Line
	5300 3650 6700 3650
Wire Wire Line
	5400 2850 5400 3750
Connection ~ 5400 3750
Wire Wire Line
	5400 3750 6800 3750
Wire Wire Line
	2000 3250 3500 3250
Wire Wire Line
	7400 2850 7400 2750
Wire Wire Line
	6000 2850 6000 2750
Wire Wire Line
	4600 2850 4600 2750
Connection ~ 7400 2750
Connection ~ 6000 2750
Connection ~ 4600 2750
$Comp
L CustomSymbols:ACSA08-51SURKWA U4
U 1 1 60230BF4
P 7100 2450
AR Path="/60230BF4" Ref="U4"  Part="1" 
AR Path="/5FEBF050/60230BF4" Ref="U?"  Part="1" 
AR Path="/5FEBF050/6289E352/60230BF4" Ref="U?"  Part="1" 
AR Path="/60258869/60230BF4" Ref="U?"  Part="1" 
F 0 "U4" H 7100 3117 50  0000 C CNN
F 1 "ACSA08-51SURKWA" H 7100 3026 50  0000 C CNN
F 2 "CustomFootprints:ACSA08-51SURKWA_SmallPad_Reverse_NoCutout" H 7100 1900 50  0001 C CNN
F 3 "https://www.kingbrightusa.com/images/catalog/SPEC/ACSA08-51SURKWA.pdf" H 7100 2450 50  0001 C CNN
	1    7100 2450
	1    0    0    -1  
$EndComp
$Comp
L CustomSymbols:ACSA08-51SURKWA U3
U 1 1 60230BFA
P 5700 2450
AR Path="/60230BFA" Ref="U3"  Part="1" 
AR Path="/5FEBF050/60230BFA" Ref="U?"  Part="1" 
AR Path="/5FEBF050/6289E352/60230BFA" Ref="U?"  Part="1" 
AR Path="/60258869/60230BFA" Ref="U?"  Part="1" 
F 0 "U3" H 5700 3117 50  0000 C CNN
F 1 "ACSA08-51SURKWA" H 5700 3026 50  0000 C CNN
F 2 "CustomFootprints:ACSA08-51SURKWA_SmallPad_Reverse_NoCutout" H 5700 1900 50  0001 C CNN
F 3 "https://www.kingbrightusa.com/images/catalog/SPEC/ACSA08-51SURKWA.pdf" H 5700 2450 50  0001 C CNN
	1    5700 2450
	1    0    0    -1  
$EndComp
$Comp
L CustomSymbols:ACSA08-51SURKWA U2
U 1 1 60230C00
P 4300 2450
AR Path="/60230C00" Ref="U2"  Part="1" 
AR Path="/5FEBF050/60230C00" Ref="U?"  Part="1" 
AR Path="/5FEBF050/6289E352/60230C00" Ref="U?"  Part="1" 
AR Path="/60258869/60230C00" Ref="U?"  Part="1" 
F 0 "U2" H 4300 3117 50  0000 C CNN
F 1 "ACSA08-51SURKWA" H 4300 3026 50  0000 C CNN
F 2 "CustomFootprints:ACSA08-51SURKWA_SmallPad_Reverse_NoCutout" H 4300 1900 50  0001 C CNN
F 3 "https://www.kingbrightusa.com/images/catalog/SPEC/ACSA08-51SURKWA.pdf" H 4300 2450 50  0001 C CNN
	1    4300 2450
	1    0    0    -1  
$EndComp
$Comp
L CustomSymbols:ACSA08-51SURKWA U1
U 1 1 60230C06
P 2900 2450
AR Path="/60230C06" Ref="U1"  Part="1" 
AR Path="/5FEBF050/60230C06" Ref="U?"  Part="1" 
AR Path="/5FEBF050/6289E352/60230C06" Ref="U?"  Part="1" 
AR Path="/60258869/60230C06" Ref="U?"  Part="1" 
F 0 "U1" H 2900 3117 50  0000 C CNN
F 1 "ACSA08-51SURKWA" H 2900 3026 50  0000 C CNN
F 2 "CustomFootprints:ACSA08-51SURKWA_SmallPad_Reverse_NoCutout" H 2900 1900 50  0001 C CNN
F 3 "https://www.kingbrightusa.com/images/catalog/SPEC/ACSA08-51SURKWA.pdf" H 2900 2450 50  0001 C CNN
	1    2900 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3550 6600 3550
Wire Wire Line
	3600 3350 5000 3350
Wire Wire Line
	1900 3150 3400 3150
$Comp
L Connector:Conn_01x04_Female J?
U 1 1 60230C0F
P 1250 1350
AR Path="/5FEBF050/6289E352/60230C0F" Ref="J?"  Part="1" 
AR Path="/60258869/60230C0F" Ref="J?"  Part="1" 
AR Path="/60230C0F" Ref="J1"  Part="1" 
F 0 "J1" H 1142 1635 50  0000 C CNN
F 1 "Conn_01x04_Female" H 1142 1544 50  0000 C CNN
F 2 "CustomFootprints:CastellatedEdge_1x04_P2.54mm" H 1250 1350 50  0001 C CNN
F 3 "~" H 1250 1350 50  0001 C CNN
	1    1250 1350
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J?
U 1 1 60230C15
P 1250 4000
AR Path="/5FEBF050/6289E352/60230C15" Ref="J?"  Part="1" 
AR Path="/60258869/60230C15" Ref="J?"  Part="1" 
AR Path="/60230C15" Ref="J2"  Part="1" 
F 0 "J2" H 1142 4285 50  0000 C CNN
F 1 "Conn_01x04_Female" H 1142 4194 50  0000 C CNN
F 2 "CustomFootprints:CastellatedEdge_1x04_P2.54mm" H 1250 4000 50  0001 C CNN
F 3 "~" H 1250 4000 50  0001 C CNN
	1    1250 4000
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J?
U 1 1 60230C1B
P 8050 1450
AR Path="/5FEBF050/6289E352/60230C1B" Ref="J?"  Part="1" 
AR Path="/60258869/60230C1B" Ref="J?"  Part="1" 
AR Path="/60230C1B" Ref="J3"  Part="1" 
F 0 "J3" H 8078 1426 50  0000 L CNN
F 1 "Conn_01x04_Female" H 8078 1335 50  0000 L CNN
F 2 "CustomFootprints:CastellatedEdge_1x04_P2.54mm" H 8050 1450 50  0001 C CNN
F 3 "~" H 8050 1450 50  0001 C CNN
	1    8050 1450
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x04_Female J?
U 1 1 60230C21
P 8050 4100
AR Path="/5FEBF050/6289E352/60230C21" Ref="J?"  Part="1" 
AR Path="/60258869/60230C21" Ref="J?"  Part="1" 
AR Path="/60230C21" Ref="J4"  Part="1" 
F 0 "J4" H 8078 4076 50  0000 L CNN
F 1 "Conn_01x04_Female" H 8078 3985 50  0000 L CNN
F 2 "CustomFootprints:CastellatedEdge_1x04_P2.54mm" H 8050 4100 50  0001 C CNN
F 3 "~" H 8050 4100 50  0001 C CNN
	1    8050 4100
	1    0    0    1   
$EndComp
Wire Wire Line
	7850 1350 6000 1350
Wire Wire Line
	7850 1250 7400 1250
Wire Wire Line
	1450 4000 2300 4000
Wire Wire Line
	1450 4100 1900 4100
Wire Wire Line
	1450 4200 1800 4200
Connection ~ 2100 2450
Wire Wire Line
	2200 2550 2200 1450
Wire Wire Line
	1450 1350 2000 1350
Connection ~ 2200 2550
Connection ~ 2000 2350
Wire Wire Line
	2500 2850 2600 2850
Connection ~ 2500 2850
Wire Wire Line
	2400 3650 2400 3900
Wire Wire Line
	1450 3900 2400 3900
Connection ~ 2400 3650
Wire Wire Line
	2300 3550 2300 4000
Connection ~ 2300 3550
Wire Wire Line
	1900 3150 1900 4100
Connection ~ 1900 3150
Wire Wire Line
	1800 3050 1800 4200
Connection ~ 1800 3050
Wire Wire Line
	3200 2850 3200 2750
Connection ~ 3200 2750
Wire Wire Line
	1450 1550 2100 1550
Wire Wire Line
	2100 1550 2100 2450
Wire Wire Line
	1450 1250 2500 1250
Wire Wire Line
	1450 1450 2200 1450
Wire Wire Line
	2000 1350 2000 2350
Wire Wire Line
	2500 1250 2500 2850
Wire Wire Line
	7800 4200 7850 4200
Wire Wire Line
	7850 4100 7800 4100
Connection ~ 7800 4100
Wire Wire Line
	7800 4100 7800 4200
Wire Wire Line
	7800 4000 7850 4000
Wire Wire Line
	7800 4000 7800 4100
$Comp
L power:GND #PWR0101
U 1 1 6023A6B2
P 7800 4250
F 0 "#PWR0101" H 7800 4000 50  0001 C CNN
F 1 "GND" H 7805 4077 50  0000 C CNN
F 2 "" H 7800 4250 50  0001 C CNN
F 3 "" H 7800 4250 50  0001 C CNN
	1    7800 4250
	1    0    0    -1  
$EndComp
Connection ~ 7800 4200
Wire Wire Line
	7800 4250 7800 4200
$Comp
L Device:LED D1
U 1 1 60285709
P 7850 2300
F 0 "D1" V 7889 2182 50  0000 R CNN
F 1 "LED" V 7798 2182 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount" H 7850 2300 50  0001 C CNN
F 3 "~" H 7850 2300 50  0001 C CNN
	1    7850 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 6028815D
P 8200 2300
F 0 "D2" V 8239 2182 50  0000 R CNN
F 1 "LED" V 8148 2182 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount" H 8200 2300 50  0001 C CNN
F 3 "~" H 8200 2300 50  0001 C CNN
	1    8200 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D3
U 1 1 60288C38
P 8550 2300
F 0 "D3" V 8589 2182 50  0000 R CNN
F 1 "LED" V 8498 2182 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount" H 8550 2300 50  0001 C CNN
F 3 "~" H 8550 2300 50  0001 C CNN
	1    8550 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D4
U 1 1 6028939E
P 8900 2300
F 0 "D4" V 8939 2182 50  0000 R CNN
F 1 "LED" V 8848 2182 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount" H 8900 2300 50  0001 C CNN
F 3 "~" H 8900 2300 50  0001 C CNN
	1    8900 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D5
U 1 1 60289D96
P 9250 2300
F 0 "D5" V 9289 2182 50  0000 R CNN
F 1 "LED" V 9198 2182 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount" H 9250 2300 50  0001 C CNN
F 3 "~" H 9250 2300 50  0001 C CNN
	1    9250 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D6
U 1 1 6028A5E0
P 9600 2300
F 0 "D6" V 9639 2182 50  0000 R CNN
F 1 "LED" V 9548 2182 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount" H 9600 2300 50  0001 C CNN
F 3 "~" H 9600 2300 50  0001 C CNN
	1    9600 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D7
U 1 1 6028AC24
P 9950 2300
F 0 "D7" V 9989 2182 50  0000 R CNN
F 1 "LED" V 9898 2182 50  0000 R CNN
F 2 "CustomFootprints:LED_Kingbright_AA3528SECKT_ReverseMount" H 9950 2300 50  0001 C CNN
F 3 "~" H 9950 2300 50  0001 C CNN
	1    9950 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 6028B52E
P 7850 2700
F 0 "R1" H 7920 2746 50  0000 L CNN
F 1 "0" H 7920 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7780 2700 50  0001 C CNN
F 3 "~" H 7850 2700 50  0001 C CNN
	1    7850 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 6028B94A
P 8200 2700
F 0 "R2" H 8270 2746 50  0000 L CNN
F 1 "0" H 8270 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8130 2700 50  0001 C CNN
F 3 "~" H 8200 2700 50  0001 C CNN
	1    8200 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 6028BD04
P 8550 2700
F 0 "R3" H 8620 2746 50  0000 L CNN
F 1 "0" H 8620 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8480 2700 50  0001 C CNN
F 3 "~" H 8550 2700 50  0001 C CNN
	1    8550 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 6028C1CF
P 8900 2700
F 0 "R4" H 8970 2746 50  0000 L CNN
F 1 "0" H 8970 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8830 2700 50  0001 C CNN
F 3 "~" H 8900 2700 50  0001 C CNN
	1    8900 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 6028C5C3
P 9250 2700
F 0 "R5" H 9320 2746 50  0000 L CNN
F 1 "0" H 9320 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9180 2700 50  0001 C CNN
F 3 "~" H 9250 2700 50  0001 C CNN
	1    9250 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 6028CAE1
P 9600 2700
F 0 "R6" H 9670 2746 50  0000 L CNN
F 1 "0" H 9670 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9530 2700 50  0001 C CNN
F 3 "~" H 9600 2700 50  0001 C CNN
	1    9600 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 6028CE82
P 9950 2700
F 0 "R7" H 10020 2746 50  0000 L CNN
F 1 "0" H 10020 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9880 2700 50  0001 C CNN
F 3 "~" H 9950 2700 50  0001 C CNN
	1    9950 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3050 7850 3050
Connection ~ 6100 3050
Wire Wire Line
	7850 3050 7850 2850
Wire Wire Line
	6200 3150 8200 3150
Wire Wire Line
	8200 3150 8200 2850
Connection ~ 6200 3150
Wire Wire Line
	6300 3250 8550 3250
Wire Wire Line
	8550 3250 8550 2850
Connection ~ 6300 3250
Wire Wire Line
	6400 3350 8900 3350
Wire Wire Line
	8900 3350 8900 2850
Connection ~ 6400 3350
Wire Wire Line
	6500 3450 9250 3450
Wire Wire Line
	9250 3450 9250 2850
Connection ~ 6500 3450
Wire Wire Line
	6600 3550 9600 3550
Wire Wire Line
	9600 3550 9600 2850
Connection ~ 6600 3550
Wire Wire Line
	6700 3650 9950 3650
Wire Wire Line
	9950 3650 9950 2850
Connection ~ 6700 3650
Wire Wire Line
	7850 2550 7850 2450
Wire Wire Line
	8200 2550 8200 2450
Wire Wire Line
	8550 2550 8550 2450
Wire Wire Line
	8900 2450 8900 2550
Wire Wire Line
	9250 2550 9250 2450
Wire Wire Line
	9600 2450 9600 2550
Wire Wire Line
	9950 2550 9950 2450
Wire Wire Line
	7850 2150 7850 2050
Wire Wire Line
	7850 2050 8200 2050
Wire Wire Line
	9950 2050 9950 2150
Wire Wire Line
	8200 2150 8200 2050
Connection ~ 8200 2050
Wire Wire Line
	8200 2050 8550 2050
Wire Wire Line
	8550 2150 8550 2050
Connection ~ 8550 2050
Wire Wire Line
	8550 2050 8900 2050
Wire Wire Line
	8900 2150 8900 2050
Connection ~ 8900 2050
Wire Wire Line
	8900 2050 9250 2050
Wire Wire Line
	9250 2150 9250 2050
Connection ~ 9250 2050
Wire Wire Line
	9250 2050 9600 2050
Wire Wire Line
	9600 2150 9600 2050
Connection ~ 9600 2050
Wire Wire Line
	9600 2050 9950 2050
Wire Wire Line
	8900 2050 8900 1850
Wire Wire Line
	8900 1850 10400 1850
Wire Wire Line
	10400 1850 10400 3800
Wire Wire Line
	10400 3800 7800 3800
Wire Wire Line
	7800 3800 7800 3900
Wire Wire Line
	7800 3900 7850 3900
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 603105AB
P 7550 4250
F 0 "#FLG0101" H 7550 4325 50  0001 C CNN
F 1 "PWR_FLAG" H 7550 4423 50  0000 C CNN
F 2 "" H 7550 4250 50  0001 C CNN
F 3 "~" H 7550 4250 50  0001 C CNN
	1    7550 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6031218D
P 7550 4250
F 0 "#PWR0102" H 7550 4000 50  0001 C CNN
F 1 "GND" H 7555 4077 50  0000 C CNN
F 2 "" H 7550 4250 50  0001 C CNN
F 3 "" H 7550 4250 50  0001 C CNN
	1    7550 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 1450 4600 2750
Wire Wire Line
	3200 1550 3200 2750
Wire Wire Line
	3200 1550 7850 1550
Wire Wire Line
	7400 1250 7400 2750
Wire Wire Line
	4600 1450 7850 1450
Wire Wire Line
	6000 1350 6000 2750
$EndSCHEMATC
